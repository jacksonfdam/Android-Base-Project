package co.mobiwise.android.baseproject.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class _BaseActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
  }
}